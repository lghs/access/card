import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AuthorizeComponent} from "./authorize/authorize.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  {path:'a', component: AuthorizeComponent},
  {path:'login', component:LoginComponent},
  {path:'', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
