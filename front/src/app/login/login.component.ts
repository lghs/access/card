import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {flatMap, map, mergeMap, Observable} from "rxjs";
import {HttpClient, HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  callbackUrl: string = ""
  servers: string[] = []

  constructor(
    private route: ActivatedRoute,
    private http:HttpClient
  ) {
  }

  ngOnInit(): void {
    this.route.queryParamMap.pipe(
      map(p => this.callbackUrl = p.get("callbackUrl") || ""),
      mergeMap(v => this.http.get<Array<string>>("/api/connect/login?callbackUrl=" + this.callbackUrl)),
    ).subscribe( r => {
      if(r.length == 1) {
        this.redirect(r[0])
      }
      this.servers = r;
    })
  }

  redirect(s: string) {
    window.location.href = "/api/connect/login?server="+ s +"&callbackUrl=" + this.callbackUrl
  }
}
