import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AuthorizationService, User, UserService} from "../../generated/api";

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.css']
})
export class AuthorizeComponent implements OnInit {
  secret: string | undefined;
  authorizationGiven = false;
  authorizationAccepted = false;

  user: User | undefined = undefined;
  userValid: boolean = false;
  userList: Array<User> = [];
  selectedUser: User | undefined;

  constructor(
    private route: ActivatedRoute,
    private authorizationService: AuthorizationService,
    private userService: UserService,
  ) {
    this.userService.getUser().subscribe(s => {
      this.user = s
      this.userValid = this.user?.cardId != undefined && this.user.cardId != ""
      if(this.user.admin){
      this.userService.getUserList().subscribe(l => this.userList = l)
      }
    });
    route.queryParams.subscribe(
      p => this.secret = p['s']
    )
  }

  ngOnInit(): void {
  }

  accept(): void {
    this.authorizationGiven = true;
    this.authorizationService.authorizeSignature(this.secret!).toPromise()
      .then(s => this.authorizationAccepted = true)
      .catch(e => {
        console.log("error :", e)
        this.authorizationGiven = false
      })

  }

  acceptOtherUser() {
    console.log("accept for ", this.selectedUser)
    this.authorizationGiven = true;
    this.authorizationService.authorizeSignatureAdmin(this.secret!, this.selectedUser).toPromise()
      .then(s => this.authorizationAccepted = true)
      .catch(e => {
        console.log("error :", e)
        this.authorizationGiven = false
      })
  }
}
