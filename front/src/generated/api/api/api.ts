export * from './accessList.service';
import { AccessListService } from './accessList.service';
export * from './authorization.service';
import { AuthorizationService } from './authorization.service';
export * from './payload.service';
import { PayloadService } from './payload.service';
export * from './user.service';
import { UserService } from './user.service';
export const APIS = [AccessListService, AuthorizationService, PayloadService, UserService];
