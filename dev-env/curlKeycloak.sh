#!/bin/bash


DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}


function  dccard(){
	docker-compose -p lghs-card -f $DIR//docker-compose.yml $@
}

export KEYCLOAK_URL=${KEYCLOAK_URL:="https:///card.lghs.local/auth/realms/master"}
export KEYCLOAK_USER=${KEYCLOAK_USER:="admin"}
export KEYCLOAK_PASS=${KEYCLOAK_PASS:="pwd"}
export KEYCLOAK_CLIENT_ID=${KEYCLOAK_CLIENT_ID:="admin-cli"}
export KEYCLOAK_CLIENT_SECRET=${KEYCLOAK_CLIENT_SECRET:=""}

function getKeycloakToken(){
	curl -k --data "username=${KEYCLOAK_USER}&password=${KEYCLOAK_PASS}&grant_type=password&client_id=${KEYCLOAK_CLIENT_ID}&client_secret=${KEYCLOAK_CLIENT_SECRET}" ${KEYCLOAK_URL}/protocol/openid-connect/token | tee /tmp/token \
	       	| sed 's/.*access_token":"//g' | sed 's/".*//g' \
		|| (
		
		if [ $1 -lt 20 ]
		then
			(>&2 wait for keycloak to be ready)
			sleep 2
			getKeycloakToken $(( $1 + 1 ))
		else
			error "time out waiting for db"
			exit 2
		fi
	) 
	(>&2 echo token result request: $(cat /tmp/token))
}




export TOKEN=$(getKeycloakToken 1)

curl -k -H "Authorization: Bearer $TOKEN" $@

