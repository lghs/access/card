#!/bin/bash

set -ue
set -o pipefail

DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}


function  dccard(){
	docker-compose -p lghs-card -f $DIR//docker-compose.yml $@
}

KEYCLOAK_URL="http://localhost:8080/realms/master"
KEYCLOAK_URL="https:///card.lghs.local/auth/admin/realms/master"

function getKeycloakToken(){
	curl -k --data "username=admin&password=pwd&grant_type=password&client_id=admin-cli" https://card.lghs.local/auth/realms/master/protocol/openid-connect/token \
	       	| sed 's/.*access_token":"//g' | sed 's/".*//g' \
		|| (
		
		if [ $1 -lt 20 ]
		then
			(>&2 wait for keycloak to be ready)
			sleep 2
			getKeycloakToken $(( $1 + 1 ))
		else
			error "time out waiting for db"
			exit 2
		fi
	)
}



info get keycloak token

TOKEN=$(getKeycloakToken 1)
info token : $TOKEN


curl -X POST -k -v $KEYCLOAK_URL/clients -H "Content-Type: application/json" -H "Authorization: bearer $TOKEN"   --data @$DIR/keycloak/card.json
curl -k -v $KEYCLOAK_URL/users -H "Content-Type: application/json" -H "Authorization: bearer $TOKEN"   --data @$DIR/keycloak/fooBarUser.json
