# LhgsCard

Backend used to sign payload of lghs cards

## dev env

add `127.0.0.1 card.lghs.local` to `/etc/hosts`

run dev env :
```bash
export PATH_TO_CARD=.../card
export UID
alias dccard="docker-compose -f $PATH_TO_CARD/dev-env/docker-compose.yml"

dccard up -d 

```

init, first time launching, wait for all container to be up then: 
```bash
./dev-env/init.sh
```

### endpoints : 
**site** https://card.lghs.local
**keycloak** https://card.lghs.local/auth/admin

### users
- admin / pwd
- foobar / pwd

