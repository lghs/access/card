#!/bin/bash
set -e

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}


test -f /app/RUNNING_PID && rm /app/RUNNING_PID 

/app/bin/card "$@"
