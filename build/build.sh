#!/bin/sh

set -ue

DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}

IMAGE_PREFIX=$1
IMAGE_TAG=$2

info build with image prefix $IMAGE_PREFIX and tag $IMAGE_TAG



info build front
export NG_CLI_ANALYTICS=ci
docker-compose -f $DIR/../dev-env/docker-compose.yml run --rm --entrypoint npm front --force install
docker-compose -f $DIR/../dev-env/docker-compose.yml run --rm front ng build --configuration=production
rm -rf $DIR/tmp || echo ""
mkdir -p  $DIR/../back/public/
cp -r $DIR/../front/dist/card/* $DIR/../back/public/

info build back
docker-compose -f $DIR/../dev-env/docker-compose.yml run --rm back "clean; dist"
rm -rf $DIR/tmp || echo ""
mkdir -p $DIR/tmp
unzip $DIR/../back/target/universal/card-*.zip -d $DIR/tmp
mv $DIR/tmp/card-* $DIR/tmp/back
cp $DIR/Dockerfile.back $DIR/tmp/Dockerfile
cp $DIR/resources/back/* $DIR/tmp/
docker build -t $IMAGE_PREFIX/card$IMAGE_TAG $DIR/tmp
