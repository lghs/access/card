
name := """card"""
organization := "be.lghs"

version := "1.0-SNAPSHOT"

maintainer := "dev@frol.be"

val generatedApiPath = file("modules/generated")

lazy val generated = (project in generatedApiPath).enablePlugins(PlayScala)

lazy val root = (project in file(".")).enablePlugins(PlayScala)
  .dependsOn(generated)
  .aggregate(generated)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      guice,
      ws,
      "com.github.jwt-scala" %% "jwt-play-json" % "9.0.2",
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
    )

  )

scalaVersion := "2.12.6"

