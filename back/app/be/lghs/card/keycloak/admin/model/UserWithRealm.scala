package be.lghs.card.keycloak.admin.model

import play.api.libs.json.Json

case class UserWithRealm(realm: String, user: User) {
  def id = user.id

  def cardId = user.cardId

  def enabled = user.enabled

  def username = user.username
}

