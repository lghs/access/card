package be.lghs.card.keycloak.admin.model

import play.api.libs.json.Json

case class Role(id: String, name: String) {
}

object Role {
  implicit val format = Json.format[Role]
}
