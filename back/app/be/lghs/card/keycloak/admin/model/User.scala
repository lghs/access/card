package be.lghs.card.keycloak.admin.model

import play.api.libs.json.Json

case class User(id: String, username: String, enabled: Boolean, attributes: Option[Map[String, Array[String]]]) {
  def cardId = attributes.getOrElse(Map()).get("cardId").flatMap(_.headOption)
}

object User {
  implicit val format = Json.format[User]
}
