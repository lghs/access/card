package be.lghs.card.keycloak.admin.model

import play.api.libs.json.Json

case class Token(access_token: String, token_type: String) {

  def headerValue=token_type + " " + access_token
}

object Token {
  implicit val format = Json.format[Token]
}
