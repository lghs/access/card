package be.lghs.card.keycloak.admin

import be.lghs.card.Conf
import be.lghs.card.keycloak.admin.model.{Role, Token, User, UserWithRealm}
import be.lghs.card.utils.JsonUtils.richJsResult
import be.lghs.card.utils.OptionUtils.enrichedObject
import play.api.{Configuration, Logging}
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}

object Requests extends Logging {

  def getAdminToken(implicit ws: WSClient, config: Configuration, executionContext: ExecutionContext) = {
    val url = Conf.keycloakAdminApiBaseUrl + "/realms/" + Conf.keycloakAdminApiAuthRealm + "/protocol/openid-connect/token"
    ws.url(url)
      .post(Map(
        "username" -> Conf.keycloakAdminApiAuthUser,
        "password" -> Conf.keycloakAdminApiAuthPass,
        "grant_type" -> "password".toOpt,
        "client_id" -> Conf.keycloakAdminApiAuthClientId,
        "client_secret" -> Conf.keycloakAdminApiAuthClientSecret,
      ).filter(_._2.isDefined).mapValues(_.get))
      .map(r => r.json.validate[Token].getOrThrow(r.json.toOpt, "error while getting token at " + url))
  }


  def users(realm:String)(implicit ws: WSClient, config: Configuration, executionContext: ExecutionContext, token: Token) : Future[List[UserWithRealm]] = {
    val url = Conf.keycloakAdminApiBaseUrl + "/admin/realms/" + realm + "/users"
    ws.url(url)
      .withHttpHeaders("Authorization" -> token.headerValue)
      .get()
      .map(r => r.json.validate[List[User]].getOrThrow(r.json.toOpt(), "error while reading users at " + url))
      .map(_.map(UserWithRealm(realm, _)))
  }

  def roles(user: UserWithRealm)(implicit ws: WSClient, config: Configuration, executionContext: ExecutionContext, token: Token): Future[List[Role]] ={
    ws.url(Conf.keycloakAdminApiBaseUrl + "/admin/realms/" + user.realm + "/users/" + user.id + "/role-mappings/realm/composite")
      .withHttpHeaders("Authorization" -> token.headerValue)
      .get()
      .map(r => r.json.validate[List[Role]].getOrThrow(r.json.toOpt()))
  }
}
