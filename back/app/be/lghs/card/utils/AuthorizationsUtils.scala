package be.lghs.card.utils

import be.lghs.card.error.AuthorizationError
import play.api.mvc.Request

object AuthorizationsUtils {

  def ifAdmin[T](f : => T)(implicit request:Request[Any]) = {
    if(request.session.get("admin").map(_.toBoolean).getOrElse(false)){
      f
    } else {
      throw new AuthorizationError("not an admin")
    }

  }

}
