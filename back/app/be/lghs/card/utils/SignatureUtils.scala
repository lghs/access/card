package be.lghs.card.utils

import play.api.Logging

import java.io.{ByteArrayOutputStream, File}
import java.nio.file.Files
import java.security.{KeyFactory, Signature}
import java.security.spec.PKCS8EncodedKeySpec
import java.util.Base64
import scala.io.Source

object SignatureUtils extends Logging {

  def sha256Rsa(input: String, strPk: String) = {
    val realPK = strPk.replaceAll("-----END PRIVATE KEY-----", "").replaceAll("-----BEGIN PRIVATE KEY-----", "").replaceAll("\n", "")
    val b1 = Base64.getDecoder.decode(realPK)
    val spec = new PKCS8EncodedKeySpec(b1)
    val kf = KeyFactory.getInstance("RSA")
    val privateSignature = Signature.getInstance("SHA256withRSA")
    privateSignature.initSign(kf.generatePrivate(spec))
    privateSignature.update(input.getBytes("UTF-8"))
    val s = privateSignature.sign
    base64(s)
  }

  def base64(s: Array[Byte]): String = {
    Base64.getEncoder.encodeToString(s)
  }

  def base64s(s: String): String = {
    base64(s.getBytes("UTF-8"))
  }
}
