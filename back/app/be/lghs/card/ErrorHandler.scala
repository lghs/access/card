package be.lghs.card

import be.lghs.card.error.{AuthentificationError, AuthorizationError}
import play.api.Logging
import play.api.http.HttpErrorHandler
import play.api.http.Status.NOT_FOUND
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.Results.{BadRequest, Forbidden, InternalServerError, Unauthorized}
import play.api.mvc.{RequestHeader, Result, Results}

import javax.inject.Singleton
import scala.concurrent.Future

case class ErrorContent(m: String, content: JsValue)
object ErrorContent{
  implicit val format = Json.format[ErrorContent]
}
@Singleton
class ErrorHandler extends HttpErrorHandler with Logging {
  import ErrorContent.format

  override def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
    if(statusCode == NOT_FOUND) {
      Future.successful(Results.NotFound)
    } else {
      Future.successful(InternalServerError(message))
    }
  }

  override def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
    exception match {
      case a: AuthentificationError => Future.successful(Unauthorized)
      case a: AuthorizationError => Future.successful(Forbidden)
    }
  }
}
