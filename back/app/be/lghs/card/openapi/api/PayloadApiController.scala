package be.lghs.card.openapi.api

import be.lghs.card.openapi.api.PayloadApi
import org.openapitools.OpenApiExceptions
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PayloadApiController @Inject()(cc: ControllerComponents, api: PayloadApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {

  def payload(): Action[AnyContent] = Action.async { request =>

    def executeApi(): Future[String] = {
      val cardId = request.getQueryString("cardId")
        .map(value => value.toLong)
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("cardId", "query string")
        }
      val secret = request.getQueryString("secret")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("secret", "query string")
        }
      api.payload(cardId, secret)(request)
    }

    executeApi().map { result =>
      Ok(result)
    }
  }
}
