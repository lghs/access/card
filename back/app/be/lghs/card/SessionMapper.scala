package be.lghs.card

import be.lghs.card.error.AuthentificationError
import be.lghs.card.openapi.model.User
import be.lghs.card.utils.OptionUtils.enrichedOption
import play.api.mvc.Request

object SessionMapper {

  def user(implicit request: Request[_]) = {
    request.session.get("userId").map { uId =>
      User(uId,
        request.session.get("userName").getOrThrowM("Name required for the user"),
        request.session.get("cardId"),
        request.session.get("admin").map(_.toBoolean))
    }.getOrThrow(new AuthentificationError())
  }
}
