package be.lghs.card

import be.lghs.card.api.{AccessListApiImpl, AuthorizationApiImpl, PayloadApiImpl, UserApiImpl}
import be.lghs.card.openapi.api.{AccessListApi, AuthorizationApi, PayloadApi, UserApi}
import play.api.inject.{Binding, Module => PlayModule}
import play.api.{Configuration, Environment}


class Module extends PlayModule {
  override def bindings(environment: Environment, configuration: Configuration): Seq[Binding[_]] = Seq(
    bind[UserApi].to[UserApiImpl],
    bind[AuthorizationApi].to[AuthorizationApiImpl],
    bind[PayloadApi].to[PayloadApiImpl],
    bind[AccessListApi].to[AccessListApiImpl]
  )
}
