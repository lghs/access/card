package be.lghs.card

import be.lghs.card.openid.OpenIdConnectConfig
import com.typesafe.config.Config
import play.api.{ConfigLoader, Configuration}

import scala.collection.JavaConverters.iterableAsScalaIterableConverter

object Conf {


  val frontRootUrl = "/"
  implicit val configLoader: ConfigLoader[List[OpenIdConnectConfig]] = {
    implicit def config2RichConfig(c:Config) = new AnyRef {
      def getStringOpt(p:String) = if(c.hasPath(p)) Option(c.getString(p)) else None
    }
    ConfigLoader(_.getConfigList).map(_.asScala.toList.map{config =>
      val c = OpenIdConnectConfig(
        name = config.getStringOpt("name").getOrElse("OpenId"),
        baseUrl = config.getString("baseUrl"),
        clientId = config.getString("clientId"),
        clientSecret = config.getStringOpt("clientSecret"),
        tokenEndpoint = config.getStringOpt("tokenEndpoint"),
      )
      if(config.hasPath("scope")) c.copy(scope=config.getString("scope")) else c
    })
  }


  def openIdConf(implicit conf: Configuration) = conf.get[List[OpenIdConnectConfig]]("openID")

  def publicKey(implicit conf: Configuration) = conf.get[String]("card.publicKey")

  def privateKey(implicit conf: Configuration) = conf.get[String]("card.privateKey")

  def authorizationExpiration(implicit conf: Configuration) = conf.getOptional[Int]("authorizationExpiration").getOrElse(60)

  def keycloakAdminApiBaseUrl(implicit conf: Configuration) = conf.get[String]("keycloak.admin.baseUrl")

  def keycloakAdminApiAuthUser(implicit conf: Configuration) = conf.getOptional[String]("keycloak.admin.auth.user")

  def keycloakAdminApiAuthPass(implicit conf: Configuration) = conf.getOptional[String]("keycloak.admin.auth.pass")

  def keycloakAdminApiAuthRealm(implicit conf: Configuration) = conf.get[String]("keycloak.admin.auth.realm")

  def keycloakAdminApiAuthClientId(implicit conf: Configuration) = conf.getOptional[String]("keycloak.admin.auth.client_id")

  def keycloakAdminApiAuthClientSecret(implicit conf: Configuration) = conf.getOptional[String]("keycloak.admin.auth.client_secret")

  def keycloakAdminApiUserRealms(implicit conf: Configuration) = conf.get[Seq[String]]("keycloak.admin.userRealms")
}
