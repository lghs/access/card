package be.lghs.card.api

import akka.http.scaladsl.model.DateTime
import be.lghs.card.error.AuthentificationError
import be.lghs.card.model.PayloadData
import be.lghs.card.openapi.api.{AuthorizationApi, PayloadApi}
import be.lghs.card.service.AuthorizationService
import be.lghs.card.utils.OptionUtils.enrichedOption
import be.lghs.card.utils.SignatureUtils
import be.lghs.card.{Conf, SessionMapper}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class PayloadApiImpl @Inject()(
                                  c: ControllerComponents,
                                  authorizationService: AuthorizationService,
                                )(
                                  implicit val ec: ExecutionContext,
                                  implicit val configuration: Configuration,
                                ) extends AbstractController(c) with PayloadApi with Logging {


  def payload(cardId: Long, secret: String)(implicit request: Request[AnyContent]): Future[String] = Future {
    val authorization = authorizationService.getAuthorization(secret).getOrThrow(new AuthentificationError())
    val b64PayloadData = SignatureUtils.base64s(Json.toJson(PayloadData(
      DateTime.now.clicks,
      cardId = cardId,
      username = authorization.username,
      userid = authorization.userCardId
    )).toString())

    b64PayloadData + "." + SignatureUtils.sha256Rsa(b64PayloadData, Conf.privateKey)
  }
}
