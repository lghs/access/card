package be.lghs.card.api

import be.lghs.card.Conf
import be.lghs.card.openid.{OpenIdConnectClient, OpenIdConnectConfig}
import be.lghs.card.utils.JsonUtils.richJsResult
import be.lghs.card.utils.OptionUtils.{enrichedObject, enrichedOption}
import be.lghs.card.utils.RequestUtils
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class IdController @Inject()(
                              val openIdClient: OpenIdConnectClient,
                              c: ControllerComponents,
                            )(
                              implicit val ec: ExecutionContext,
                              val conf: Configuration,
                            ) extends AbstractController(c) with Logging {

  lazy val config = Conf.openIdConf

  def login = Action.async { implicit request =>
    val c = serverConf(request)
    c.map { serverConf =>
      openIdClient
        .redirectURL(serverConf, redirectUrl(serverConf))
        .map(url => Redirect(url))
    }.getOrElse(Future(Ok(Json.toJson(config.map(_.name)))))

  }


  private def serverConf(implicit request: Request[AnyContent]) = {
    config.find(_.name.toOpt == request.queryString.get("server").flatMap(_.headOption))
  }

  private def redirectUrl(conf: OpenIdConnectConfig)(implicit request: Request[AnyContent]) = {
    routes.IdController.openIdCallback
      .absoluteURL(RequestUtils.isSecure(request)) + callbackUrl.map(s"?server=${conf.name}&callbackUrl=" + _).getOrElse("")
  }

  private def callbackUrl(implicit request: Request[AnyContent]) = {
    request.queryString.get("callbackUrl").flatMap(_.headOption)
  }

  def openIdCallback = Action.async { implicit request: Request[AnyContent] =>

    def rolesFor(token: JsValue, context: String) = {
      (token \ "resource_access" \ context \ "roles").validate[Array[String]].getOrLog(token.toOpt(), "error while reading roles in token for context " + context).getOrElse(Array())
    }

    def isAdmin(token: JsValue) = {
      val allRoles = rolesFor(token, "account") ++ rolesFor(token, serverConf.map(_.clientId).getOrThrowM("Missing client id"))
      logger.debug("all roles " + allRoles)
      allRoles
        .contains("card-admin")
    }

    val c = serverConf(request).getOrThrowM("Missing server info")
    openIdClient.getToken(c, redirectUrl(c), request.queryString.get("code").flatMap(_.headOption).getOrThrowM("missing code"))
      .map { t => logger.debug("token : " + t); t }
      .map(t => isAdmin(t) -> t.validate[OpenConnectId](OpenConnectId.format).getOrThrow())
      .map { case (isAdmin, v) => Redirect(callbackUrl.getOrElse(Conf.frontRootUrl)).withSession(
        "userId" -> v.sub,
        "userName" -> v.username,
        "cardId" -> v.cardId.getOrElse(""),
        "admin" -> isAdmin.toString)
      }
  }
}

case class OpenConnectId(iss: String, sub: String, preferred_username: String, cardId: Option[String]) {
  def username = preferred_username
}

object OpenConnectId {
  implicit val format = Json.format[OpenConnectId]
}
