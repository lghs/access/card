package be.lghs.card.api

import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import be.lghs.card.{Conf, SessionMapper}
import be.lghs.card.error.AuthentificationError
import be.lghs.card.keycloak.admin.Requests
import be.lghs.card.openapi.api.UserApi
import be.lghs.card.openapi.model.User
import be.lghs.card.utils.AuthorizationsUtils.ifAdmin
import be.lghs.card.utils.OptionUtils.enrichedOption
import play.api.libs.ws.WSClient
import play.api.{Configuration, Logging}
import play.api.mvc._

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class UserApiImpl @Inject()(
                             c: ControllerComponents,
                           )(
                             implicit val ec: ExecutionContext,
                             conf:Configuration,
                             ws:WSClient,
                             mat: Materializer,
) extends AbstractController(c) with UserApi with Logging {
  override def getUser()(implicit request: Request[AnyContent]): Future[User] = {
    Future(SessionMapper.user)
  }

  override def getUserList()(implicit request: Request[AnyContent]): Future[List[User]] = {
    ifAdmin{

      Requests.getAdminToken.flatMap { implicit token =>
        logger.debug("users list token " + token + " for source " + Conf.keycloakAdminApiUserRealms.toList)
        Source(Conf.keycloakAdminApiUserRealms.toList)
          .flatMapConcat(r => Source.future(Requests.users(r)).flatMapConcat(Source(_)))
          .filter(u => u.enabled && u.cardId.isDefined)
          .map(u => User(u.id, u.username, u.cardId, None))
          .runWith(Sink.collection)
          .map(_.sortBy(_.name).toList
          )
      }
    }
  }
}
