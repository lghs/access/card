package be.lghs.card.api

import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import be.lghs.card.Conf
import be.lghs.card.keycloak.admin.Requests
import be.lghs.card.openapi.api.AccessListApi
import play.api.libs.ws.WSClient
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class AccessListApiImpl @Inject()(
                                   c: ControllerComponents,
                                 )(
                                   implicit val ec: ExecutionContext,
                                   configuration: Configuration,
                                   ws: WSClient,
                                   mat: Materializer,
                                 ) extends AbstractController(c) with AccessListApi with Logging {
  override def accessListFor(authorization: String)(implicit request: Request[AnyContent]): Future[List[String]] = {

    Requests.getAdminToken.flatMap { implicit token =>
      Source(Conf.keycloakAdminApiUserRealms.toList)
        .flatMapConcat(r => Source.future(Requests.users(r)).flatMapConcat(Source(_)))
        .filter(u => u.enabled && u.cardId.isDefined)
        .mapAsync(10)(u => Requests.roles(u).map(rs => u -> rs))
        .filter(_._2.exists(_.name == authorization))
        .map(_._1.cardId.get)
        .runWith(Sink.collection)
        .map(_.sorted.toList
        )
    }
  }
}
