package be.lghs.card.api

import be.lghs.card.Conf
import be.lghs.card.openid.{OpenIdConnectClient, OpenIdConnectConfig}
import be.lghs.card.utils.JsonUtils.richJsResult
import be.lghs.card.utils.OptionUtils.enrichedOption
import be.lghs.card.utils.RequestUtils
import play.api.libs.json.Json
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject.Inject
import scala.concurrent.ExecutionContext

class CertificateController @Inject()(
                              c: ControllerComponents,
                            )(
                              implicit val ec: ExecutionContext,
                              val conf: Configuration,
                            ) extends AbstractController(c) with Logging {

  def pub = Action { implicit request =>
    Ok(Conf.publicKey)
  }
}
