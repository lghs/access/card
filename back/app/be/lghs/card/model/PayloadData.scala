package be.lghs.card.model

import play.api.libs.json.Json

case class PayloadData(iad:Long, cardId:Long, username: String, userid: String) {

}

object PayloadData{
  implicit val format = Json.format[PayloadData]
}
