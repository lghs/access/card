package be.lghs.card.service

import akka.http.scaladsl.model.DateTime
import be.lghs.card.Conf
import be.lghs.card.service.AuthorizationService.authorizations
import com.typesafe.play.cachecontrol.HeaderNames.Authorization
import play.api.Configuration

import javax.inject.Inject
import scala.collection.mutable

class AuthorizationService @Inject()(implicit configuration: Configuration){
  def getAuthorization(secret:String) = {
    AuthorizationService.clean()
    authorizations.get(secret)
  }

  def authorize(secret:String, userCardId:String, username:String) {
    val authorization = AuthorizationService.Authorization(secret, userCardId, username, DateTime.now)
    authorizations.put(authorization.secret, authorization)
  }
}

object AuthorizationService {


  case class Authorization(secret:String, userCardId:String, username:String, time:DateTime){
    def isExpired(implicit configuration: Configuration) = (time + (Conf.authorizationExpiration * 1000) ) < DateTime.now
  }

  val authorizations : mutable.Map[String, Authorization] = mutable.Map()

  def clean()(implicit configuration: Configuration) = authorizations
    .filter(_._2.isExpired)
    .foreach(v => authorizations.remove(v._1))

}
