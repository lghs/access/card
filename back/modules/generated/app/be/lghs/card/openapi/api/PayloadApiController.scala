package be.lghs.card.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class PayloadApiController @Inject()(cc: ControllerComponents, api: PayloadApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * GET /api/payload/?cardId=[value]&secret=[value]
    */
  def payload(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[String] = {
      val cardId = request.getQueryString("cardId")
        .map(value => value.toLong)
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("cardId", "query string")
        }
      val secret = request.getQueryString("secret")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("secret", "query string")
        }
      api.payload(cardId, secret)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
