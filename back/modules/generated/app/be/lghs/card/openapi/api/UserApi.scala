package be.lghs.card.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.lghs.card.openapi.model.User

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait UserApi {
  /**
    * get connected user
    */
  def getUser()(implicit request:Request[AnyContent]): Future[User]


  /**
    * get user list
    */
  def getUserList()(implicit request:Request[AnyContent]): Future[List[User]]

}
