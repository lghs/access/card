package be.lghs.card.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.lghs.card.openapi.model.User

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class AuthorizationApiController @Inject()(cc: ControllerComponents, api: AuthorizationApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * GET /api/authorize/?secret=[value]
    */
  def authorizeSignature(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      val secret = request.getQueryString("secret")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("secret", "query string")
        }
      api.authorizeSignature(secret)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  /**
    * PUT /api/authorize/admin?secret=[value]
    */
  def authorizeSignatureAdmin(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[Unit] = {
      val user = request.body.asJson.map(_.as[User])
      val secret = request.getQueryString("secret")
        .getOrElse {
          throw new OpenApiExceptions.MissingRequiredParameterException("secret", "query string")
        }
      api.authorizeSignatureAdmin(secret, user)(request)
    }

    executeApi().map { _ =>
      Ok
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
