package be.lghs.card.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class AccessListApiController @Inject()(cc: ControllerComponents, api: AccessListApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * GET /api/access-list/:authorization
    */
  def accessListFor(authorization: String): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[String]] = {
      api.accessListFor(authorization)(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
