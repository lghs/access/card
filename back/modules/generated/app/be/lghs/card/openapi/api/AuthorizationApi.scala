package be.lghs.card.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import be.lghs.card.openapi.model.User

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait AuthorizationApi {
  /**
    * authorize the server to respond to a request with card payload
    */
  def authorizeSignature(secret: String)(implicit request:Request[AnyContent]): Future[Unit]


  /**
    * authorize the server to respond to a request with card payload for another user
    */
  def authorizeSignatureAdmin(secret: String, user: Option[User])(implicit request:Request[AnyContent]): Future[Unit]

}
