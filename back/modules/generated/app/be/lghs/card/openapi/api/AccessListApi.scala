package be.lghs.card.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait AccessListApi {
  /**
    * get all user card id that have a specific authorization
    */
  def accessListFor(authorization: String)(implicit request:Request[AnyContent]): Future[List[String]]

}
