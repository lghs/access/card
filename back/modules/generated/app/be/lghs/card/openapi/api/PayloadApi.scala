package be.lghs.card.openapi.api

import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
trait PayloadApi {
  /**
    * get payload to write on card
    */
  def payload(cardId: Long, secret: String)(implicit request:Request[AnyContent]): Future[String]

}
