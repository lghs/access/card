package be.lghs.card.openapi.api

import org.openapitools.OpenApiExceptions
import javax.inject.{Inject, Singleton}
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.{ExecutionContext, Future}
import be.lghs.card.openapi.model.User

@javax.annotation.Generated(value = Array("org.openapitools.codegen.languages.ScalaPlayFrameworkServerCodegen"))
@Singleton
class UserApiController @Inject()(cc: ControllerComponents, api: UserApi)(implicit executionContext: ExecutionContext) extends AbstractController(cc) {
  /**
    * GET /api/user
    */
  def getUser(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[User] = {
      api.getUser()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * GET /api/user/list
    */
  def getUserList(): Action[AnyContent] = Action.async { request =>
    def executeApi(): Future[List[User]] = {
      api.getUserList()(request)
    }

    executeApi().map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  private def splitCollectionParam(paramValues: String, collectionFormat: String): List[String] = {
    val splitBy =
      collectionFormat match {
        case "csv" => ",+"
        case "tsv" => "\t+"
        case "ssv" => " +"
        case "pipes" => "|+"
      }

    paramValues.split(splitBy).toList
  }
}
